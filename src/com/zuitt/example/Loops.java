package com.zuitt.example;

public class Loops {
    public static void main(String[] args) {
        //For loops
        for(int i = 0; i < 10; i++){
            //int i = 0 -> initial condition
            //i < 10 -> limiting expression
            //i++ -> increment/decrement
            System.out.println("Current count: " + i);
        }
        //For loops can be used to loop through the contents of an array
        int[] intArray = {100,200,300,400,500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }
        //Another way to loop through arrays is called the foreach or the enhanced for loop
        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for(String name : nameArray){//This gets each element of the array and assigns it to the variable called "name"
            System.out.println(name);
        }

        //Nested for loops
        //Multidimensional Array
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for(int row = 0; row < 3; row++){//Outer for loop will loop through the rows
            for(int col = 0; col < 3; col++){//Inner loop will loop through the columns in each row
                System.out.println(classroom[row][col]);
            }
        }

        //While and Do-while loops
        //While loops allow for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate through us indefinite
        //Do-while loops are similar to while loops. However, do-while loops will always execute at least once - while loops may not execute at all

        int x = 0;
        int y = 10;

        while(x < 10){
            System.out.println("Loop number: " + x);
            x++;
        }

        do{
            System.out.println("Countdown: " + y);
        }while (y > 10);

    }
}
